def jip():
    print('집')
    
    
from tqdm import tqdm
from tensorflow.keras.preprocessing.image import load_img
from tensorflow.keras.preprocessing.image import img_to_array

def loadAndCropImg(commonPath, pathSeries, width, height, leftTopROIXSeries, leftTopROIYSeries, rightBottomROIXSeries, rightBottomROIYSeries):
    imgList = []
    
    # NOTE: tqdm 클래스로 진행률을 표시할 수 있다.
    for i in tqdm(range(len(pathSeries))):
        img = load_img(commonPath + pathSeries[i])
        img = img.crop(
            (leftTopROIXSeries[i], leftTopROIYSeries[i]
            ,rightBottomROIXSeries[i], rightBottomROIYSeries[i])
        )
        img = img.resize(
            (width, height)
        )
        
        imgList.append(img_to_array(img))
        
    return imgList

def loadImg(commonPath, pathSeries, width, height):
    imgList = []
    
    # NOTE: tqdm 클래스로 진행률을 표시할 수 있다.
    for i in tqdm(range(len(pathSeries))):
        img = load_img(commonPath + pathSeries[i])
        img = img.resize(
            (width, height)
        )
        
        imgList.append(img_to_array(img))
        
    return imgList

import matplotlib.pyplot as plt
from tensorflow.keras.preprocessing.image import array_to_img

def printPrediction(features, labels, predictions):
    plt.figure(figsize=(13, 13))
    
    length = len(features)
    if (25 < length):
        length = 25

    for i in range(length):
        plt.subplot(5, 5, i + 1)
        plt.grid(False)
        plt.xticks([])
        plt.yticks([])

        col = 'g'
        if (labels[i] != predictions[i]):
            col = 'r'

        plt.xlabel('Actual={}, Prediction={}'.format(labels[i], predictions[i]), color=col)
        plt.imshow(array_to_img(features[i]))

import tensorflow as tf

def saveTFLiteModel(model, filePath):
    converter = tf.lite.TFLiteConverter.from_keras_model(model)
    convertedModel = converter.convert()
    with open(filePath, 'wb') as f:
        f.write(convertedModel)